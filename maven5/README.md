# Creating the Basic Maven Project in Eclipse

## Creating the Project

** Process to create a maven web application in eclipse **

1. Open Eclipse
2. New Maven Project
3.  Select ArticraftID as your application name
4. Artifact=maven-archetype-webapp
5. Save

## Creating properties file for enviornment setup
1. Create profiles in maven and making development profile as default
2. By using profiles we are able to change the properties file for different environments
	
3. After that go to the folder where pom.xml contains and do 
	mvn package then it will generate war file and keeps the properties file in war depending on the profile we have selected
	
4. deploying webApp.war	
	1. First go the tomcat webapps folder and paste it
	2. go to tomcat->bin folder start tomcat by clicking startup.bat
	3. go to browser write localhost:port/project name(localhost:8013/assignment5-0.0.1-SNAPSHOT/index.jsp)
		here i am running the tomcat on 8013 port
		
#### Learning from assignment

1. Creating a Maven Project WebApplication From Eclipse
2. Build the maven Project
3. Run the Main Class of Project
4. Run the Test Cases Of the Project
5. Adding Dependencies to project by using `<dependency></dependency>` tag of pom.xml
6.  Specifying the version of dependencies in pom.xml
7. deploying of war file in tomcat and starting of it