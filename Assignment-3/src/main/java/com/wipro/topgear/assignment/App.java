package com.wipro.topgear.assignment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.commons.lang3.StringUtils;

/**
 * Hello world!
 *
 */
public class App 
{
	private static App app=new App();
	
    public static void main( String[] args )
    {
        System.out.println( "Length of \"Hello World!\" is "+app.length("Hello World!") );
        try {
        	Class.forName("org.hsqldb.jdbcDriver");
        	Connection con=DriverManager.getConnection("jdbc:hsqldb:mem:.", "SA", "");
        	PreparedStatement pst=con.prepareStatement("create table assignments(name Varchar(25),score Integer)");
        	pst.executeUpdate();
        	pst=con.prepareStatement("insert into assignments values('MAVEN',90)");
        	pst.executeUpdate();
        	pst=con.prepareStatement("insert into assignments values('Spring',90)");
        	pst.executeUpdate();
        	pst=con.prepareStatement("select * from assignments");
        	ResultSet rs=pst.executeQuery();
        	while(rs.next())
        	{
        		System.out.println(rs.getString(1)+" : "+rs.getInt(2));
        	}
        }catch(Exception e)
        {
        	e.printStackTrace(System.out);
        }
    }
    public int length(String string){
  		return StringUtils.length(string);	
      }
}
