# Creating the Basic Maven Project in Eclipse

## Creating the Project

** Process to create a maven web application in eclipse **

1. Open Eclipse
2. New Maven Project
3.  Select ArticraftID as your application name
4. Artifact=maven-archetype-webapp
5. Save

## Learings From this Assignment

1. Creating a Maven Project From Eclipse
2. Build the maven Project
3. Run the Main Class of Project
4. Run the Test Cases Of the Project
5. Opening the maven project in eclipse
6. Adding Dependencies to project by using `<dependency></dependency>` tag of pom.xml
7.  Specifying the version of dependencies in pom.xml
 