# Creating the Basic Maven Project in Command Line

## Creating the Project

** Enter the following command in command line to create basic maven project with required artifactid and groupid **

```
mvn archetype:generate "-DgroupId=com.wipro.topgear" "-DartifactId=basicMaven" "-DarchetypeArtifact=maven-archetype-quickstart" "-DinteractiveMode=false"

```
** Enter the Following command to build the project **
```
mvn package

```
** Enter the Following command to Run the main class **
```
java -cp target/basicMaven-1.0-SNAPSHOT.jar com.wipro.topgear.App
```

** Enter the Following commant to run test cases **
```
mvn test -Dtest=AppTest
```

## Learings From this Assignment

1. Creating a Maven Project From Command Line
2. Build the maven Project
3. Run the Main Class of Project
4. Run the Test Cases Of the Project