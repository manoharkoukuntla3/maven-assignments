package com.wipro.topgear;
import org.apache.commons.lang3.StringUtils;
/**
 * Hello world!
 *
 */
public class App 
{
   public static void main( String[] args )
    {
        System.out.println("The length of \"Length Should be 19\" is " + new App().length("Length Should be 19") );
    }
    
    public int length(String string){
		return StringUtils.length(string);	
    }
}
